Ext.define('RZDApp.widgets.paramForUsuriisk.windowParams',  {

    getWindowParams: function(props) {

       return new Ext.XTemplate(
           '<div>',
               '<table class="windowParams">',
                   '<tr>',
                       '<th><button id="buttonSave" class="buttonSave">СОХРАНИТЬ</button>ПАРАМЕТРЫ</th>',
                       '<th>БЛЮХЕР</th>',
                       '<th>ПОСЬЕТ</th>',
                       '<th>КАМЫШОВАЯ</th>',
                       '<th>ХАСАН</th>',
                   '</tr>',
                   '<tr>',
                       '<td>Количество вагонов в составе поезда после отцепки вагонов</td>',
                       '<td><input id="bluher_in_1" type="number" name="quantity" value="'+document.getElementById('bluher_out_1').textContent+'" min="0" max="100"></td>',
                       '<td><input id="pocet_in_1" type="number" name="quantity" value="'+document.getElementById('pocet_out_1').textContent+'" min="0" max="100"></td>',
                       '<td><input id="kamish_in_1" type="number" name="quantity" value="'+document.getElementById('kamish_out_1').textContent+'" min="0" max="100"></td>',
                       '<td><input id="hasan_in_1" type="number" name="quantity" value="'+document.getElementById('hasan_out_1').textContent+'" min="0" max="100"></td>',
                   '</tr>',
                   '<tr>',
                       '<td>Количество вагонов в составе поезда, сформированного из отцепленных вагонов</td>',
                       '<td><input id="bluher_in_2" type="number" name="quantity" value="'+document.getElementById('bluher_out_2').textContent+'" min="0" max="100"></td>',
                       '<td><input id="pocet_in_2" type="number" name="quantity" value="'+document.getElementById('pocet_out_2').textContent+'" min="0" max="100"></td>',
                       '<td><input id="kamish_in_2" type="number" name="quantity" value="'+document.getElementById('kamish_out_2').textContent+'" min="0" max="100"></td>',
                       '<td><input id="hasan_in_2" type="number" name="quantity" value="'+document.getElementById('hasan_out_2').textContent+'" min="0" max="100"></td>',
                   '</tr>',
                   '<tr>',
                       '<td>Время формирования поезда из отцепленных вагонов (час)</td>',
                       '<td><input id="bluher_in_3" type="number" name="quantity" value="'+document.getElementById('bluher_out_3').textContent+'" min="0" max="100"></td>',
                       '<td><input id="pocet_in_3" type="number" name="quantity" value="'+document.getElementById('pocet_out_3').textContent+'" min="0" max="100"></td>',
                       '<td><input id="kamish_in_3" type="number" name="quantity" value="'+document.getElementById('kamish_out_3').textContent+'" min="0" max="100"</td>',
                       '<td><input id="hasan_in_3" type="number" name="quantity" value="'+document.getElementById('hasan_out_3').textContent+'" min="0" max="100"></td>',
                   '</tr>',
               '</table>',
           '</div>'
       );
    }
});
