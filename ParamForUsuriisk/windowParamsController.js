Ext.define('RZDApp.widgets.paramForUsuriisk.windowParamsController',  {

    constructor: function(RepColumns) {
        if (RepColumns) {
            this.RepColumns = RepColumns;
            this.getController();
        }
    },

    getController: function() {
        var me = this,
            RepColumns = me.RepColumns;

        for (var i = 0; i < RepColumns.length; i++) {
            RepColumns[i].renderer = function (value, meta, record, rowIndex, colIndex, store, view) {
                if (meta) {
                    var isLocked = view.headerCt.columnManager.headerCt.lockedCt; // если есть заблокированная колонка

                    var newStyle = 'style="';


                    if (colIndex === 0  && isLocked) {

                        if (rowIndex === 0 || rowIndex === 2 || rowIndex === 4 || rowIndex === 6 || rowIndex === 8) {

                            newStyle += 'font-weight: bold;';
                        }
                    }

                    var rowspanText = "";
                    newStyle += '"; ';
                    meta.tdAttr += newStyle + rowspanText;
                }
                return value;
            };
            RepColumns[i].renderer.column = RepColumns[i];
        }
    }

});
