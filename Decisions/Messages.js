Ext.define('RZDApp.widgets.Decisions.Messages', {

    getMessages: function(props) {
        let me = this;
        let { random_id, el_id } = props;

        let html_data = me.getHTMLText(props)

        let messages = Ext.create('Ext.Container', {
            renderTo: el_id,
            xtype: 'panel',
            items: [{
                xtype: 'container',
                html: html_data
            }],
        });

        let onClick = document.getElementById(random_id);

        onClick.addEventListener('click', function() {
            me.windowDecision(me.getHTMLText(props, 'ON_CLICK_DECISION'))
        });

        return messages;
    },

    getHTMLText: function (props, click) {

        let me = this;
        let { triggerId, random_id, state, fact_train_number, train_index, reason, caption_name, data } = props;
        let fontSize = click === 'ON_CLICK_DECISION' ? 'font-size: 18px;' : 'font-size: 14px;';

        return '<div id="' + triggerId + '" style="margin: 10px; padding: 10px; border: 1px solid black; text-align: -webkit-center;">'
        + '<div id="' + random_id + '" style="' + fontSize + ' font-weight: bold; text-align: center; margin-bottom: 10px; padding: 10px;'
        + me.setBackgroundColor(state) + '">' // FFDB8B желтый;  pink; BDECB6 - зеленый;
        + '<span style="color: red;">'
        + 'Номер поезда: ' + fact_train_number + ' Индекс: ' + train_index
        + '</br>'
        + reason + caption_name + '</span></br>'
        + '</br>'
        + data
        + '</div>'
        + '</div>';
    },

    setBackgroundColor: function (props) {
        let backgroundColor;
        switch (props) {
            case 0:
                backgroundColor =  'background-color: greenyellow;';
                break;
            case 1:
                backgroundColor =  'background-color: #bcffcf;';
                break;
            case 2:
                backgroundColor =  'background-color: #ff9797;';
                break;
            case 3:
                backgroundColor =  'background-color: yellow;';
                break;
            case 4:
                backgroundColor =  'background-color: pink;';
                break;
        }
        return backgroundColor;
    },

    windowDecision: function(target) {
        return Ext.create('Ext.window.Window', {
            autoScroll: true,
            width: '600px',
            modal: true,
            padding: 10,
            title: '<br>&nbsp;<br>&nbsp;',
            cls: 'table-grid',
            html: target
        }).show();
    },
});
