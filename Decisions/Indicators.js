Ext.define('RZDApp.widgets.Decisions.Indicators', {

    getIndicators: function(props) {
        var me = this;

        props.word = ['ЛЦ', 'Д', 'Т'];
        props.numberOne = [0, 1, 2];
        props.me = me;

        return Ext.create('Ext.Container', {
            renderTo: document.getElementById(props.trigger_id),
            defaults: {
                xtype: 'button',
            },
            layout: {
                type: 'hbox',
                align: 'center',
                pack: 'center'
            },
            items: [
                me.setButtonIndicator(props.word[0], props.numberOne[0], props),
                me.setButtonIndicator(props.word[1], props.numberOne[1], props),
                me.setButtonIndicator(props.word[2], props.numberOne[2], props),
            ]
        });
    },

    setButtonIndicator: function (word, numberOne, props) {

        return {
            text: word,
            width: '70px',
            style: {
                border: '1px solid black',
                padding: 0,
                margin: '10px 2px'
            },
            listeners: {
                afterrender: function (btn) {
                    props.btn = btn;
                    props.btn.getEl().down('.x-btn-inner').setStyle({'color': '#000'});
                    for (var i = 0; i < props.bg_colors.length; i++) {
                        props.btn.getEl().down('.x-btn-inner').setStyle({'background-color': props.bg_colors[props.trigger.roles[numberOne][1]]});
                    }
                }
            }
        }
    }
});
