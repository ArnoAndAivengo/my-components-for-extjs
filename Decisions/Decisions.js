Ext.define('RZDApp.widgets.Decisions.Decisions', {

    requires: [
        'RZDApp.widgets.Decisions.Indicators',
        'RZDApp.widgets.Decisions.Messages',
        'RZDApp.widgets.Decisions.Buttons'
    ],

    getDecision: function(props) {

        let { decision, el_id, fact_train_number, train_index } = props;
        let bg_colors = ['grey', 'green', 'red', 'yellow'];

        if ( decision === null || decision === undefined ) { return }

        for (var i = 0; i < decision.length; i++) {

            let dec = decision[i];

            Ext.Function.defer( function () {
                Ext.Array.each(dec, function (value, index) {

                    let {decisions, reason} = value;
                    let {info, trigger, trigger_id} = decisions[index];
                    let {state, caption_name, data} = info[index] === null || undefined ? false : info[index];
                    let triggerId = trigger_id ? trigger_id + Math.floor(Math.random(100000, 1) * 10000) : Math.floor(Math.random(100000, 1) * 10000);
                    let random_id = Math.floor(Math.random(10000, 1) * 10020);

                    caption_name = caption_name === null || caption_name === undefined || false ? "" : caption_name;
                    fact_train_number = fact_train_number === null || fact_train_number === undefined ? "" : fact_train_number;
                    train_index = train_index === null || train_index === undefined ? "" : train_index;
                    reason = reason === null || reason === undefined ? "" : reason;
                    data = data === null || data === undefined ? "" : data;

                    let messages_data = {
                        triggerId,
                        random_id,
                        state,
                        fact_train_number,
                        train_index,
                        reason,
                        caption_name,
                        data,
                        el_id
                    };

                    if (info[index] !== null || info[index] !== undefined) {
                        Ext.create('RZDApp.widgets.Decisions.Messages').getMessages(messages_data);
                    }

                    props.trigger_id = triggerId;
                    props.trigger = trigger;
                    props.bg_colors = bg_colors;

                    if (trigger && props.is_button) {
                        let indicators = Ext.create('RZDApp.widgets.Decisions.Indicators').getIndicators(props);
                        Ext.create('RZDApp.widgets.Decisions.Buttons').getButtons(props, indicators);
                    }

                });
            }, 50);
        }
        return Ext.String.format('<div id="{0}"></div>', props.el_id);
    }

});
