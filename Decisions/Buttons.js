Ext.define('RZDApp.widgets.Decisions.Buttons', {

    getButtons: function(props, indicators) {
        var me = this;

        props.text = ['Утв. ЛЦ', 'Отк. ЛЦ'];
        props.backgroundColor = ['#bcffcf', '#ff9797'];
        props.border = ['green', 'red'];
        props.number = [1, 2];
        props.method = ['approve_decision', 'reject_decision'];
        props.me = me;

        return Ext.create('Ext.Container', {
            renderTo: document.getElementById(props.trigger_id),
            defaults: {
                xtype: 'button',
                margin: 5,
            },
            layout: {
                type: 'hbox',
                align: 'center',
                pack: 'center'
            },
            style: {
                color: '#000',
            },
            items: [
                me.setButtonIndicator(props, props.text[0], props.backgroundColor[0], props.border[0], props.method[0], props.number[0], indicators),
                me.setButtonIndicator(props, props.text[1], props.backgroundColor[1], props.border[1], props.method[1], props.number[1], indicators),

            ]
        });
    },

    setButtonIndicator: function (props, text, backgroundColor, border, method, number, indicators) {

        return {
            text: text,
            cls: 'indicatorsButtons',
            style: {
                'border': '1px solid ' + border,
                'background-color': '' + backgroundColor,
            },
            listeners: {
                afterrender: function (btn) {
                    btn.getEl().down('.x-btn-inner').setStyle({'color': '#000'});
                }
            },
            handler: function (btn) {
                props.me.decision_making(props, method, number, indicators)
            }
        }
    },

    decision_making: function(props, method, number, indicators) {
        Ext.Ajax.request({
            method: 'GET',
            async: true,
            url: Ext.isTestMode ? 'resources/json/control/' + method + '.json' : '/ajax.php?module=KUP&method=' + method + '&id=' + props.trigger.id,
            success: function (response) {
                var responseText = Ext.JSON.decode(response.responseText);

                if (responseText.data === true) {
                    switch (props.trigger.approve_step) {
                        case 0:
                            indicators.items.items[0].btnInnerEl.dom.style.backgroundColor = props.bg_colors[number];
                            break;
                        case 1:
                            indicators.items.items[1].btnInnerEl.dom.style.backgroundColor = props.bg_colors[number];
                            break;
                        case 2:
                            indicators.items.items[2].btnInnerEl.dom.style.backgroundColor = props.bg_colors[number];
                            break;
                    }
                }
            }
        });
    }
});
