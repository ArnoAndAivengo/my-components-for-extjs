Ext.define('RZDApp.widgets.buttonsBlock.buttonsBlock',  {
    getButtonsBlock: function(props) {

        return Ext.create('Ext.Container', {
            style: {
                backgroundColor: '#b6936f',
                'margin': '0 10px',
            },
            items: [
                {
                    xtype: "container",
                    style: {
                        'border':'1px solid black',
                        backgroundColor: '#b6936f',
                        'padding': '5px',
                    },
                    items: [
                        Ext.create('RZDApp.widgets.buttonsBlock.text').getText(),
                        Ext.create('RZDApp.widgets.buttonsBlock.buttons').getButtons(props)
                    ]
                }
            ]
        });
    }
});
