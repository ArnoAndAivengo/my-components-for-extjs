Ext.define('RZDApp.widgets.buttonsBlock.buttons',  {

    getButtons: function(props) {
        var self = this,
            buttons = [];

        Ext.Array.each(props, function(item){
            buttons.push(
                self.request_container(
                    item
            ))
        });

        return Ext.create('Ext.Container', {
            cls: 'buttonsBlock-buttons',
            layout: {
                type: 'hbox',
                align: 'center',
                pack: 'bottom'
            },
            style: {
                backgroundColor: '#b6936f',
            },
            items: [
                {
                    xtype: "container",
                    layout: {
                        type: 'hbox'
                    },
                    items: buttons
                }
            ]
        });
    },

    request_dislocation(title, url, nameStation) {
        Ext.create('RZDApp.view.control.AccessToPorts.Operators.WindowOperators.WindowOperators_1', {
            title: Ext.String.format(
                title
            ),
            url: Ext.isFakeData ? 'resources/json/control/kup_train_car_dislocation_detail.json' : url,
            extraParams: {
                textButton: nameStation
            }
        }).show();
    },

    request_container(props) {
        var self = this;

        return {
            xtype: "container",
            defaults: {
                layout: {
                    type: "vbox"
                },
            },
            items: [
                {
                    xtype: "container",
                    layout: {
                        type: 'vbox',
                        align: 'center',
                        pack: 'bottom'
                    },

                    items: [
                        {
                            xtype: 'button',
                            text: '',
                            cls: 'buttonBlocksLabel',
                            handler: function ( me, html, obj ) {

                                var text = '';
                                if (props.nameStation === 'КАМЫШОВАЯ' || props.nameStation === 'ХАСАН') {
                                    text = 'ПЕРЕЧЕНЬ ПОЕЗДОВ СДАННЫХ по ст. '
                                } else {
                                    text = 'ПЕРЕЧЕНЬ ПОЕЗДОВ РАСФОРМИРОВАННЫХ по ст. '

                                }
                                self.request_dislocation(
                                    RZDApp.Utils.setTitle( text, props.nameStation),
                                    props.url,
                                    props.nameStation
                                )
                            },
                            listeners: {
                                afterrender: function (me, html, obj) {
                                    Ext.Ajax.request({
                                        url: props.url,
                                        async: false,
                                        method: "GET",
                                        success: function (response) {
                                            if (response) {
                                                var responseData = Ext.JSON.decode(response.responseText);
                                                me.setText('<span style="font-size: 14px; color: yellow;">' + responseData.data.length  + '</span>');
                                            }
                                        },
                                        failure: function (error) {
                                            console.log(error);
                                        }
                                    });
                                }
                            }
                        },
                        {
                            xtype: 'label',
                            html: '<div style="font-weight: bold">' + props.nameStationBtn + '</div>',
                            cls: 'buttonBlocks',
                            // handler: function ( me, html, obj ) {
                            //     var text = '';
                            //     if (textButton === 'БЛЮХЕР' || textButton === 'ПОСЬЕТ') {
                            //         text = 'ПЕРЕЧЕНЬ ПОЕЗДОВ РАСФОРМИРОВАННЫХ по ст. '
                            //     } else {
                            //         text = 'ПЕРЕЧЕНЬ ПОЕЗДОВ СДАННЫХ по ст. '
                            //     }
                            //     self.request_dislocation(
                            //         '<div style="display: inline-flex;">' +
                            //         '<p style="color: #000; font-size: 16px;">' + text +
                            //         '<span style="font-size: 22px; color: blue;">' + textButton + '</span>' +
                            //         '</p>' +
                            //         '</div>',
                            //         url,
                            //         textButton
                            //     )
                            // }
                        }
                    ]
                }
            ]
        }
    }

});
