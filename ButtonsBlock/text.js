Ext.define('RZDApp.widgets.buttonsBlock.text',  {

    getText: function(buttonValues) {

        var min = Ext.Date.add(new Date(), Ext.Date.MINUTE, -2520);
        var max = Ext.Date.add(new Date(), Ext.Date.MINUTE, -1070);
        min.setDate(min.getDate() + 1);
        max.setDate(max.getDate() + 1);

        return {
            xtype: 'label',
            html: '<div class="getText">РАСФОРМИРОВАНО <br> с 18:01 ' + Ext.Date.format(min, 'd.m.Y') + ' до 18:00 ' + Ext.Date.format(new Date(max), 'd.m.Y') + '</div>',
        }
    }

});